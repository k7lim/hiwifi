package com.hiwifi;

import android.os.Bundle;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.squareup.otto.Bus;

import javax.inject.Inject;


public abstract class AbstractBaseActivity extends SherlockFragmentActivity {
    @Inject
    Bus bus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((HiWiFiApplication) getApplication()).inject(this);
    }
}
