package com.hiwifi;

import android.os.Bundle;
import com.actionbarsherlock.app.SherlockFragment;
import com.squareup.otto.Bus;

import javax.inject.Inject;

public abstract class AbstractBaseFragment extends SherlockFragment {
    @Inject
    Bus bus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((HiWiFiApplication)getActivity().getApplication()).inject(this);
    }
}
