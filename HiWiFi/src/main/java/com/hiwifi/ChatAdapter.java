package com.hiwifi;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.Views;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.firebase.client.Firebase;

import javax.inject.Inject;

/**
 * Created by newuser on 7/21/13.
 *     public ChatListAdapter(Query ref, Activity activity, int layout, String username) {
 super(ref, Chat.class, layout, activity);
 this.username = username;
 }
 */
public class ChatAdapter extends AbstractFirebaseAdapter<Post> {

    private final String username;

    @Inject
    ImageLoader imageLoader;

    @InjectView(R.id.image_author)
    NetworkImageView mAuthorImage;

    @InjectView(R.id.text_author)
    TextView mAuthorName;

    @InjectView(R.id.text_body)
    TextView mText;

    public ChatAdapter(Firebase ref, Activity activity, int layout, String username) {
        super(ref, Post.class, layout, activity);
        this.username = username;
    }

    @Override
    protected void populateView(View v, Post model) {
        Views.inject(this, v);

        mText.setText(model.getText());
        mAuthorName.setText(model.getAuthor());
        if(TextUtils.equals(model.getAuthor(), username)) {
            mAuthorName.setTextSize(mAuthorName.getTextSize() + 2);
        }
        mAuthorImage.setImageUrl(model.getImgurl(), imageLoader);
    }
}
