package com.hiwifi;

import android.annotation.TargetApi;
import android.net.wifi.WifiConfiguration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class CommunityActivity extends AbstractBaseActivity  {
    public static final String EXTRA_CONFIG = "com.hiwifi.config";
    private static final String TAG_PEOPLE = "People";
    private static final String TAG_POSTS = "Posts";
    private static final String TAG_PAINT = "Paint";

    private WifiConfiguration mConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community);

        Bundle args = savedInstanceState == null ? getIntent().getExtras() : savedInstanceState;
        mConfig = args.getParcelable(EXTRA_CONFIG);

        if (savedInstanceState == null) {
            CommunityFragment communityFragment = CommunityFragment.newInstance(mConfig);
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, communityFragment).commit();
        }


        setTitle(mConfig.SSID.replace('"', ' '));
        // Show the Up button in the action bar.
        setupActionBar();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        for (int i = 0; i < 3; i++) {
            ActionBar.Tab tab = getSupportActionBar().newTab();

            if (i == 0) {
                tab.setText(TAG_PEOPLE);
            } else if (i == 1) {
                tab.setText(TAG_POSTS);
            } else { //i == 2
                tab.setText(TAG_PAINT);
            }
            tab.setTag(i);
            tab.setTabListener(new ActionBar.TabListener() {
                @Override
                public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                    bus.post(tab);
                }

                @Override
                public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                }

                @Override
                public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                }
            });

            getSupportActionBar().addTab(tab);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.community, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(EXTRA_CONFIG, mConfig);
    }
}
