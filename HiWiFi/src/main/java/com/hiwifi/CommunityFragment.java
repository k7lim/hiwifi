package com.hiwifi;

import android.net.wifi.WifiConfiguration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.InjectView;
import butterknife.Views;
import com.actionbarsherlock.app.ActionBar;
import com.squareup.otto.Subscribe;

/**
 * Created by newuser on 7/21/13.
 */
public class CommunityFragment extends AbstractBaseFragment {

    public static final String KEY_ARG_CONFIG = "com.hiwifi.args.config";

    @InjectView(R.id.pager_community_tabs)
    ViewPager pager;

    public static CommunityFragment newInstance(WifiConfiguration config) {
        CommunityFragment result = new CommunityFragment();

        Bundle args = new Bundle(1);
        args.putParcelable(KEY_ARG_CONFIG, config);

        result.setArguments(args);

        return result;
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        bus.unregister(this);
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_community_pager, container, false);
        Views.inject(this, result);

        final WifiConfiguration config = getArguments().getParcelable(KEY_ARG_CONFIG);
        pager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                Fragment fragment;
                if (i == 0) {
                    fragment = CommunityPeopleFragment.newInstance(config);
                } else if (i == 1) {
                    fragment = CommunityPostsFragment.newInstance(config);
                } else { //i == 2
                    fragment = CommunityPaintFragment.newInstance(config);
                }

                return fragment;
            }

            @Override
            public int getCount() {
                return 3;
            }
        });
        return result;
    }

    @Subscribe
    public void onTabSelected(ActionBar.Tab tab) {
        Integer tag = (Integer) tab.getTag();
        if (tag == null) {
            pager.setCurrentItem(0, true);
        } else {
            pager.setCurrentItem(tag, true);
        }
    }
}
