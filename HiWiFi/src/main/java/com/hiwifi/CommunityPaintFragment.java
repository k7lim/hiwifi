package com.hiwifi;

import android.net.wifi.WifiConfiguration;
import android.os.Bundle;

/**
 * Created by newuser on 7/21/13.
 */
public class CommunityPaintFragment extends AbstractBaseFragment {
    public static CommunityPaintFragment newInstance(WifiConfiguration config) {
        CommunityPaintFragment result = new CommunityPaintFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(CommunityFragment.KEY_ARG_CONFIG, config);
        return result;
    }
}
