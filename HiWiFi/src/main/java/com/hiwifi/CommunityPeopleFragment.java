package com.hiwifi;

import android.net.wifi.WifiConfiguration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import butterknife.InjectView;
import butterknife.Views;
import com.firebase.client.Firebase;

/**
 * Created by newuser on 7/21/13.
 */
public class CommunityPeopleFragment extends AbstractBaseFragment {

    @InjectView(R.id.people_grid)
    GridView peopleGrid;

    public static CommunityPeopleFragment newInstance(WifiConfiguration config) {
        CommunityPeopleFragment result = new CommunityPeopleFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(CommunityFragment.KEY_ARG_CONFIG, config);
        return result;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_community_people, container, false);
        Views.inject(this, result);

        Firebase ref = new Firebase("https://hiwifi.firebaseio.com/rooms/0/users");
        peopleGrid.setAdapter(new PeopleAdapter(ref, getActivity(), R.layout.griditem_person));

        return result;
    }
}
