package com.hiwifi;

import android.net.wifi.WifiConfiguration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import butterknife.InjectView;
import butterknife.Views;
import com.android.volley.toolbox.NetworkImageView;
import com.firebase.client.Firebase;
import com.firebase.client.Query;

import javax.inject.Inject;

/**
 * Created by newuser on 7/21/13.
 *
 *   @Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
View result = inflater.inflate(R.layout.fragment_community_people, container, false);
Views.inject(this, result);

peopleGrid.setAdapter(new PeopleAdapter(query, this, R.layout.griditem_person, username));

return result;


 */
public class CommunityPostsFragment extends AbstractBaseFragment {

    @InjectView(R.id.chat_list)
    ListView chatList;

    @InjectView(R.id.image_chat_me)
    NetworkImageView meImage;

    @InjectView(R.id.edit_message)
    EditText textBox;

    @InjectView(R.id.button_send)
    ImageButton sendButton;

    @Inject
    User meUser;

    public static CommunityPostsFragment newInstance(WifiConfiguration config) {
        CommunityPostsFragment result = new CommunityPostsFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(CommunityFragment.KEY_ARG_CONFIG, config);
        return result;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_community_chat, container, false);
        Views.inject(this, result);

        Firebase ref = new Firebase("https://hiwifi.firebaseio.com/rooms/0/posts");
        ListAdapter adapter = new ChatAdapter(ref, getActivity(), R.layout.listitem_chatpost, meUser.getName());
        chatList.setAdapter(adapter);

        return result;
    }
}
