package com.hiwifi;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import com.actionbarsherlock.app.SherlockDialogFragment;


public class HelpDialogFragment extends SherlockDialogFragment {

    public static final String TAG = "HelpDialogFragment";

    public static HelpDialogFragment newInstance() {
        return new HelpDialogFragment();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_help_title);
        builder.setView(getContentView());
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss(); // don't care which button!
            }
        });
        Dialog dialog = builder.create();
        return dialog;
    }

    private View getContentView() {
        View result = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_help, null, false);
        return result;
    }
}