package com.hiwifi;

import android.app.Application;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.squareup.otto.Bus;
import dagger.Module;
import dagger.ObjectGraph;
import dagger.Provides;

import javax.inject.Singleton;

public class HiWiFiApplication extends Application {
    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();

        objectGraph = ObjectGraph.create(new HiWiFiModule(this));
    }

    public void inject(Object object) {
        objectGraph.inject(object);
    }

    @Module(
            injects={ MainActivity.class, WifiPickerFragment.class,
                    CreatePersonaActivity.class, CommunityActivity.class,
                    CommunityFragment.class, CommunityPostsFragment.class,
                    CommunityPeopleFragment.class, CommunityPaintFragment.class,
                    ChatAdapter.class, PeopleAdapter.class  }
    )
    static class HiWiFiModule {
        private static final int BMP_CACHE_BYTES = 1024 * 1024 * 2;
        private final HiWiFiApplication application;

        public HiWiFiModule(HiWiFiApplication hiWiFiApplication) {
            this.application = hiWiFiApplication;
        }

        @Provides @Singleton
        Bus provideBus() {
            return new Bus();
        }

        @Provides @Singleton
        RequestQueue provideRequestQueue() {
            return Volley.newRequestQueue(application);
        }

        @Provides @Singleton
        ImageLoader.ImageCache provideImageCache() {
            return new ImageLoader.ImageCache() {

                private final LruCache<String, Bitmap> lruCache = new LruCache<String, Bitmap>(BMP_CACHE_BYTES) {
                    @Override
                    protected int sizeOf(String key, Bitmap value) {
                        return value.getRowBytes() * value.getHeight();
                    }
                };

                @Override
                public Bitmap getBitmap(String key) {
                    return lruCache.get(key);
                }

                @Override
                public void putBitmap(String key, Bitmap bitmap) {
                    lruCache.put(key, bitmap);
                }
            };
        }

        @Provides @Singleton
        ImageLoader provideImageLoader(RequestQueue queue, ImageLoader.ImageCache imageCache) {
            return new ImageLoader(queue, imageCache);
        }

        @Provides @Singleton
        User provideMeUser() {
            return new User("McGee", "http://i.imgur.com/YPzW7xXl.jpg");
        }
    }
}
