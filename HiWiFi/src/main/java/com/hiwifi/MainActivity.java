package com.hiwifi;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;


import java.util.Collections;
import java.util.List;

public class MainActivity extends AbstractBaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            List<WifiConfiguration> configs = (wifi == null) ? Collections.<WifiConfiguration>emptyList() : wifi.getConfiguredNetworks();
            WifiPickerFragment pickerFragment = WifiPickerFragment.newInstance(configs);
            getSupportFragmentManager()
                    .beginTransaction().add(android.R.id.content, pickerFragment).commit();
        }

        setTitle(getString(R.string.select_communities));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            showHelpDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showHelpDialog() {
        HelpDialogFragment.newInstance().show(getSupportFragmentManager(), HelpDialogFragment.TAG);
    }

}
