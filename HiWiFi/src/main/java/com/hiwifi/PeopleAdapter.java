package com.hiwifi;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.Views;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.firebase.client.Firebase;

import javax.inject.Inject;

/**
 * Created by newuser on 7/21/13.
 *     public ChatListAdapter(Query ref, Activity activity, int layout, String username) {
 super(ref, Chat.class, layout, activity);
 this.username = username;
 }
 */
public class PeopleAdapter extends AbstractFirebaseAdapter<User> {

    @Inject
    ImageLoader imageLoader;

    @InjectView(R.id.image_author)
    NetworkImageView mAuthorImage;

    @InjectView(R.id.text_author)
    TextView mAuthorName;

    public PeopleAdapter(Firebase ref, Activity activity, int layout) {
        super(ref, User.class, layout, activity);
    }

    @Override
    protected void populateView(View v, User model) {
        Views.inject(this, v);

        mAuthorName.setText(model.getName());
        mAuthorImage.setDefaultImageResId(R.drawable.default_person);
        mAuthorImage.setErrorImageResId(R.drawable.default_person);
        mAuthorImage.setImageUrl(model.getImgurl(), imageLoader);
    }
}
