package com.hiwifi;

public class Post {

    private String text;
    private String author;
    private String imgurl;

    // Required default constructor for Firebase object mapping
    private Post() { }

    Post(String text, String author, String imgurl) {
        this.text = text;
        this.author = author;
        this.imgurl = imgurl;
    }
    public String getText() {
        return text;
    }

    public String getAuthor() {
        return author;
    }

    public String getImgurl() {
        return imgurl;
    }
}
