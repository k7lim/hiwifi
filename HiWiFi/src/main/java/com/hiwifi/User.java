package com.hiwifi;

import javax.inject.Inject;

/**
 * Created by newuser on 7/21/13.
 */
public class User {

    public String getName() {
        return name;
    }

    public String getImgurl() {
        return imgurl;
    }

    private String name;
    private String imgurl;

    private User() {    }

    User(String name, String imgurl) {
        this.name = name;
        this.imgurl = imgurl;
    }

}
