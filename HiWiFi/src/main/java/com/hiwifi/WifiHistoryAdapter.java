package com.hiwifi;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class WifiHistoryAdapter extends ArrayAdapter<WifiConfiguration> {
    private final List<WifiConfiguration> mSelected;
    private final View.OnTouchListener mOnTouchListener;


    public WifiHistoryAdapter(Context context, List<WifiConfiguration> configs, List<WifiConfiguration> selected, View.OnTouchListener touchListener) {
        super(context, 0, configs == null ? new ArrayList<WifiConfiguration>() : configs);
        mSelected = selected == null ? new ArrayList<WifiConfiguration>() : selected;

        mOnTouchListener = touchListener;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).networkId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View result;
        final WifiConfiguration config = getItem(position);
        if (convertView == null) {
            result = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_wifinetwork, parent, false);
        } else {
            result = convertView;
        }
        final TextView label = (TextView) result.findViewById(R.id.text_ssid);

        result.setOnTouchListener(mOnTouchListener);
        result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent communityIntent = new Intent(v.getContext(), CommunityActivity.class);
                communityIntent.putExtra(CommunityActivity.EXTRA_CONFIG, config);
                v.getContext().startActivity(communityIntent);
            }
        });
        result.setTag(config);

        label.setText(config.SSID.replace('"', ' '));
        return result;
    }


    public List<WifiConfiguration> getSelected() {
        return mSelected;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
